﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeanInstatiate : MonoBehaviour
{
    [SerializeField]
    public GameObject LeanManager;

    private void Awake() {
        if(DontDestroy.Instance == null) {
            Instantiate(LeanManager,transform.position,Quaternion.Euler(0,0,0));
        }
    }
}
