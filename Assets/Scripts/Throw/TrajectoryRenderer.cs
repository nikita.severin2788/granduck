﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryRenderer : MonoBehaviour
{
    private LineRenderer lineRendererComponent;
    public int alinement = 1;
    bool rendering = false;

	private void Awake()
	{
        lineRendererComponent = GetComponent<LineRenderer>();
    }
	void Start()
    {
        HideTrajectory();
    }

    public void ShowTrajectory(Vector3 origin, Vector3 speed)
    {
        speed = speed / alinement;

        if (!rendering)
        {
            rendering = true;
            lineRendererComponent.enabled = true;
        }

        Vector3[] points = new Vector3[100];
        lineRendererComponent.positionCount = points.Length;

        for (int i = 0; i < points.Length; i++)
        {
            float time = i * 0.1f;

            points[i] = origin + speed * time + Physics.gravity * time * time / 2f;

            if(points[i].y < -1)
            {
                lineRendererComponent.positionCount = i + 1;
                break;
            }
        }

        lineRendererComponent.SetPositions(points);
    }

    public void HideTrajectory()
    {
        rendering = false;
        lineRendererComponent.enabled = false;
    }
}
