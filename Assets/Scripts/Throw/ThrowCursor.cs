﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThrowCursor : MonoBehaviour
{
    public static event UnityAction NotEnough;
    public static event UnityAction RestartButtonActivate;
    public static event UnityAction<int> SetStoneCount;
    public static event UnityAction<bool> NewThrowActive;
    public delegate void ThrowTargetTransrom(Transform transform);
    public event ThrowTargetTransrom TargetTransform;


    [Header("Stats")]
    [SerializeField] private float maxStretch;
    [SerializeField] private float minForce;
    [SerializeField] private float maxForce;
    [SerializeField] private float minJumpForce;
    [SerializeField] private float maxJumpForce;
    [SerializeField] private AnimationCurve forceCurve;

    [Header("Values")]
    [SerializeField] private float distance;
    [SerializeField] private int stoneAmount;
    [SerializeField] private LayerMask mask;
    [SerializeField] private float oppDirectionDebug;

    [Header("Transforms")]
    [SerializeField] private Transform arrow;
    [SerializeField] private Transform helper;

    [Header("GameObjects")]
    [SerializeField] private GameObject stonePrefab;
    [SerializeField] private TrajectoryRenderer trajectoryRenderer;

    [Header("Line")]
    [SerializeField] private LineRenderer line;

    private Vector3 direction;
    private Vector3 lastCursorPos;
    private GameObject curStone;
    private bool setting;
    private bool aiming;

    RaycastHit hit;

    private bool ThrowAiming
    {
        get => aiming;
        set 
        {
            aiming = value;
            NewThrowActive?.Invoke(aiming);
        }
    }

    public int StoneAmount
    {
        get => stoneAmount;
        set 
        {
            stoneAmount = value;
            SetStoneCount?.Invoke(StoneAmount);
        }
    }

    private void Start()
    {
        Setting();
        //SetStoneCount.Invoke(StoneAmount);
        //LevelManager.Singleton.PauceEvent += ThrowOnOff;
    }

    private void OnEnable()
    {
        Bed.DedInBad += Enabled;
        IDamageble.GameOver += Enabled;
        Plane.AddStone += AddStone;
        SwipeController.Swipe += ThrowOff;
        CameraRig.Rotation += ThrowOff;
        Bed.DedInBad -= ThrowOff;
        IDamageble.GameOver -= ThrowOff;
    }

    private void OnDisable()
    {
        Bed.DedInBad -= Enabled;
        IDamageble.GameOver -= Enabled;
        Plane.AddStone -= AddStone;
        SwipeController.Swipe -= ThrowOff;
        CameraRig.Rotation -= ThrowOff;
        Bed.DedInBad -= ThrowOff;
        IDamageble.GameOver -= ThrowOff;

        setting = false;
        ThrowAiming = false;
        helper.gameObject.SetActive(false);
        arrow.gameObject.SetActive(false);
        line.gameObject.SetActive(false);
        trajectoryRenderer.HideTrajectory();
    }

    private void ThrowOff()
    {
        if (ThrowAiming)
        {
            trajectoryRenderer.HideTrajectory();
            ThrowAiming = false;
            line.gameObject.SetActive(false);

            StoneAmount++;

            Invoke("Setting", 0.2f);
            Destroy(curStone);
        }
    }

    //private void ThrowOnOff(bool state)
    //{
    //    enabled = state;
    //}

    private void Enabled()
    {
        enabled = false;
        ThrowOff();
    }

    private void AddStone()
    {
        StoneAmount++;
    }

    private void Setting()
    {
        arrow.gameObject.SetActive(true);
        helper.gameObject.SetActive(false);
        setting = true;
        ThrowAiming = false;
    }

    void Update()
    {
        if (ThrowAiming)
        {
            if (Input.GetMouseButtonUp(0))
                Shoot(GetOppDirection());
        }

        if (!ToWorld(helper, false))
            return;

        if (setting)
        {
            ToWorld(arrow, true);
            if (Input.GetMouseButtonDown(0)) Aim();
        }
        if (ThrowAiming)
        {
            if (Input.GetMouseButton(0))
                Aiming(GetOppDirection());

            if (Input.GetMouseButtonUp(0))
                Shoot(GetOppDirection());
        }

    }

    private Vector3 GetOppDirection()
    {
        float value = distance / maxStretch;
        float force = Mathf.Lerp(minForce, maxForce, forceCurve.Evaluate(value));
        float jumpForce = Mathf.Lerp(minJumpForce, maxJumpForce, value);

        Vector3 oppDirection = new Vector3(direction.x * -1 * force, jumpForce, direction.z * -1 * force);

        oppDirectionDebug = Vector3.Magnitude(oppDirection);
        return oppDirection;
    }

    private void Aim()
    {
        if (StoneAmount > 0)
        {
            StoneAmount--;

            setting = false;
            ThrowAiming = true;
            helper.position = arrow.position;
            helper.gameObject.SetActive(true);
            arrow.gameObject.SetActive(false);

            Vector3 pos = new Vector3(arrow.position.x, arrow.position.y + 2, arrow.position.z);
            curStone = Instantiate(stonePrefab, arrow.position, Quaternion.identity);

            RestartButtonActivate?.Invoke();
        }
        else
        {
            //Not enough sound
            NotEnough?.Invoke();
        }
    }

    private void Aiming(Vector3 oppDirection)
    {
        trajectoryRenderer.ShowTrajectory(curStone.transform.position, oppDirection);

        line.gameObject.SetActive(true);
        line.SetPosition(0, curStone.transform.position);
        distance =  Vector3.Distance(arrow.position, helper.position);

        direction = helper.position - arrow.position;
        direction.Normalize();

        curStone.transform.LookAt(helper.position);
        curStone.transform.Rotate(0, 180, 0);

        TargetTransform(curStone.transform);

        if (distance > maxStretch)
        {
            Vector3 point = arrow.position + direction * maxStretch;
            line.SetPosition(1, point);
        }
        else line.SetPosition(1, helper.position);
    }

    private void Shoot(Vector3 oppDirection)
    {
        trajectoryRenderer.HideTrajectory();
        ThrowAiming = false;
        line.gameObject.SetActive(false);
        
        if (Vector3.Magnitude(oppDirection) > 180)
            curStone.GetComponent<Duck>().Launch(oppDirection);
        else
        {
            Destroy(curStone);
            StoneAmount++;
        }

        Invoke("Setting", 0.2f);
    }

    public bool ToWorld(Transform trans, bool onPlane)
    {
        //if (EventSystem.current.IsPointerOverGameObject())
        //    return false;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.SphereCast(ray, 0.5f, out hit, float.MaxValue, mask))
        {
            trans.position = hit.point;
            return true;
        }
        return false;
    }

    public bool OnSet()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.SphereCast(ray, 0.5f, out hit, float.MaxValue, mask))
        {
            return true;
        }
        return false;
    }
}