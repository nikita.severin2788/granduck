﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Granduck.Audio;
public class Duck : MonoBehaviour
{
    public static UnityAction<Vector3> SetTarget;

    [SerializeField] private float stopSpeed = 1f;
    [SerializeField] private float bounce = 1f;
    [SerializeField] private Footsteps _hitSound;

    private Rigidbody RB;
    private bool checkVelocity = false;
    private bool signal = true;



    public bool Signal { get => signal; set => signal = value; }

    public void ActivateTarget()
    {
        Signal = false;
        SetTarget?.Invoke(transform.position);
    }

    private void OnEnable()
    {
        checkVelocity = false;
        RB = GetComponent<Rigidbody>();

        PhysicMaterial material = GetComponent<BoxCollider>().material;
        material.bounciness = bounce;
    }

    public void Launch(Vector3 oppDirection)
    {
        RB.constraints = RigidbodyConstraints.None;
        RB.AddForce(oppDirection);
        //RB.angularVelocity = new Vector3(Random.Range(1000, 10000), Random.Range(1000, 10000), Random.Range(1000, 10000));
    }

    private void FixedUpdate()
    {
        if (signal == true)
        {
            if (!checkVelocity && Vector3.Magnitude(RB.velocity) > stopSpeed)
            {
                checkVelocity = true;
            }

            if (checkVelocity && Vector3.Magnitude(RB.velocity) < stopSpeed)
            {
                RB.velocity = Vector3.zero;
                SetTarget?.Invoke(transform.position);
                signal = false;
            }
        }
    }
    

    private void OnCollisionEnter(Collision collision)
    {

        //Debug.Log(collision.transform.name);

        RB.velocity = RB.velocity * bounce;

        var SF = collision.gameObject.GetComponent<DuckTarget>();
        if (SF != null && signal)
        {
            SF.Touch(transform);
            signal = false;

            if (SF.signal)
            {
                SetTarget?.Invoke(collision.gameObject.transform.position);

                var gram = collision.gameObject.GetComponent<Gramophone>();
                if (gram != null) gram.Fade();
            }
        }
        else _hitSound.Step();
    }
}
