﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ThrowUI : MonoBehaviour
{

    [SerializeField] private GameObject notEnoughText;
    public GameObject restartButton;
    

    private void OnEnable()
    {
        ThrowCursor.SetStoneCount += UpdateText;
        ThrowCursor.NotEnough += NotEnough;
        ThrowCursor.RestartButtonActivate += RestartButtonActivate;
    }

    private void OnDisable()
    {
        ThrowCursor.SetStoneCount -= UpdateText;
        ThrowCursor.NotEnough -= NotEnough;
        ThrowCursor.RestartButtonActivate -= RestartButtonActivate;
    }

    private void UpdateText(int stoneAmount)
    {
        Granduck.UI.GameUI.instance.Ducks = stoneAmount;
    }

    private void NotEnough()
    {
        notEnoughText.SetActive(true);
    }

    private void RestartButtonActivate()
    {
        restartButton.SetActive(true);
    }
}
