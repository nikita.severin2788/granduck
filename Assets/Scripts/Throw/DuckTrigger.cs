﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DuckTrigger : MonoBehaviour
{
    public static event UnityAction StonePunch;
    public Transform ded;

    private void Update()
    {
        if (ded != null)
        {
            transform.position = ded.transform.position;
            transform.rotation = ded.transform.rotation;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        var Stone = collision.gameObject.GetComponent<Duck>();
        if (Stone != null)
        {
            if (Stone.Signal == true)
            {
                Stone.Signal = false;
                StonePunch?.Invoke();
                collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }
}
