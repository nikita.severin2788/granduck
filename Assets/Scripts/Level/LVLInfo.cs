﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LVLInfo : MonoBehaviour
{
    public int levelID;
    public int duckFor2Stars;
    public int duckFor3Stars;

    private int stones = 0;

    private void OnEnable()
    {
        Bed.DedInBad += SaveGame;
        ThrowCursor.SetStoneCount += SetStoneCount;
    }

    private void OnDisable()
    {
        Bed.DedInBad -= SaveGame;
        ThrowCursor.SetStoneCount -= SetStoneCount;
    }

    private void SetStoneCount(int stones)
    {
        this.stones = stones;
    }

    private void SaveGame()
    {
        if (levelID != 0)
        {
            if(stones >= duckFor3Stars)
                GetComponent<SaveManager>().SaveLevel(levelID, 3);
            else if (stones >= duckFor2Stars)
                GetComponent<SaveManager>().SaveLevel(levelID, 2);
            else
                GetComponent<SaveManager>().SaveLevel(levelID, 1);
        }
        else
            Debug.Log("Данные для сохранения не заданы");
    }
}
