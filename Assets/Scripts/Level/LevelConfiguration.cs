﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConfiguration : MonoBehaviour
{
    [SerializeField] public GameObject[] ConfigurationPrefab = new GameObject[1];

    private void Start()
    {
        int LevelIndex = Random.Range(0, ConfigurationPrefab.Length);
        ConfigurationPrefab[LevelIndex].SetActive(true);
    }



}
