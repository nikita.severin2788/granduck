﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Singleton;

    public delegate void PauseMode(bool State);
    public event PauseMode PauceEvent;

    [Header("PC Panels")]
    public GameObject PCWinPanel;
    public GameObject PCGameOverPanel;
    public GameObject PCPaucePanel;
    public GameObject PCCounter;
    public GameObject PCStoneText;


    [Header("PC Panels")]
    public GameObject MobileWinPanel;
    public GameObject MobileGameOverPanel;
    public GameObject MobilePaucePanel;
    public GameObject MobileCounter;
    public GameObject MobileStoneText;

    [Header ("GameUI")]
    [SerializeField]
    //private GameObject _restartButton;

    
    
    [Header("Values")]
    public int LoadSceneIndex;
    
    private bool _isDead;    
    private bool _isWin;
    private bool _isPauseMode = true;



    private void Awake()
    {
        Singleton = this;
        
    }



    private void OnEnable()
    {
        Bed.DedInBad += WinGame;
        IDamageble.GameOver += GameOver;
    }
    
    private void OnDisable()
    {
        Bed.DedInBad -= WinGame;
        IDamageble.GameOver -= GameOver;
    }

    private void Update()
    {
        if(!_isDead && !_isDead)
        if (Input.GetKeyDown(KeyCode.Escape)){
                if (_isPauseMode)
                    Pauce();
                else
                    Resume();
        }
    }

    IEnumerator OpenMenu(float delay, GameObject menu)
    {
        yield return new WaitForSeconds(delay);
        menu.SetActive(true);
    }

    public void GameOver()
    {
        if (!_isWin && !_isDead)
        {
            _isDead = true;
            PCStoneText.SetActive(false);
            //_restartButton.SetActive(false);
            StartCoroutine(OpenMenu(1f, PCGameOverPanel));
        }
    }

    public void WinGame()
    {
        if (!_isDead && !_isWin)
        {
            _isWin = true;
            PCStoneText.SetActive(false);
            //_restartButton.SetActive(false);
            StartCoroutine(OpenMenu(1f, PCWinPanel));
        }
    }

    public void Pauce()
    {
        PauceEvent(true);
        PCPaucePanel.SetActive(true);
        PCCounter.SetActive(false);
        _isPauseMode = false;
        Time.timeScale = 0f;
    }
    public void Resume()
    {
        PauceEvent(false);
        PCPaucePanel.SetActive(false);
        PCCounter.SetActive(false);
        _isPauseMode = true;
        Time.timeScale = 1f;
    }



    public void CloseAllPanel()
    {
        PCGameOverPanel.SetActive(true);
        PCWinPanel.SetActive(true);
    }

    public void RestartGame()
    {
        Debug.Log("NextScene");
        SceneManager.LoadScene(0);        
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(LoadSceneIndex);
    }
}
