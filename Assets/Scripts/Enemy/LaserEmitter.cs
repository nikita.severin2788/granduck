﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEmitter : MonoBehaviour
{
    public GameObject laserObject;

    void Start()
    {
        var laserGO = Instantiate(laserObject, transform);
        laserGO.transform.rotation = transform.rotation;
        var laser = laserGO.GetComponent<Laser>();
        laser.LaserUpdate(transform.right);
    }

    void Emition()
    {
        StartCoroutine(Emition2());
    }

    IEnumerator Emition2()
    {
        yield return new WaitForFixedUpdate();
        var laserGO = Instantiate(laserObject, transform);
        laserGO.transform.rotation = transform.rotation;
        var laser = laserGO.GetComponent<Laser>();
        laser.LaserUpdate(transform.right);
    }

    private void OnEnable()
    {
        Laser.RestartLaser += Emition;
    }

    private void OnDisable()
    {
        Laser.RestartLaser -= Emition;
    }
}
