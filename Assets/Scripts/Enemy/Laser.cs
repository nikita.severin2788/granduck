﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Granduck.Audio;
public class Laser : MonoBehaviour
{
    public static UnityAction RestartLaser;
    private static UnityAction OffLaser;

    public LineRenderer LR;
    [SerializeField] private LayerMask mask;
    [SerializeField] private GameObject _sparksPrefab;
    private Vector3 dir;

    public void LaserUpdate(Vector3 direction)
    {
        if (!LR.enabled)
            LR.enabled = true;

        dir = direction;

        RaycastHit hit;
        Ray ray = new Ray(transform.position, direction);
        Physics.Raycast(ray, out hit, mask);
        
        if (hit.collider != null)
        {
            Debug.DrawLine(ray.origin, hit.point, Color.red);

            var m = hit.collider.gameObject.GetComponent<Mirror>();
            var point = m != null ? m.point.position : hit.point;

            Vector3[] points = new Vector3[] { transform.position, point };
            LR.SetPositions(points);

            var mirror = hit.collider.gameObject.GetComponent<Mirror>();
            if (mirror != null)
            {
                mirror.LaserHit(transform.position, hit.point);
            }
        }
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, dir);
        Physics.Raycast(ray, out hit, mask);

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                hit.collider.GetComponent<IDamageble>().SetDamage();
                Instantiate(_sparksPrefab, hit.transform.position, Quaternion.identity);
                OffLaser?.Invoke();
            }
        }
    }

    private void OnEnable()
    {
        RestartLaser += LaserOff;
        OffLaser += LaserOff;
    }

    private void OnDisable()
    {
        RestartLaser -= LaserOff;
        OffLaser -= LaserOff;
    }

    void LaserOff()
    {
        Destroy(gameObject);
    }
}
