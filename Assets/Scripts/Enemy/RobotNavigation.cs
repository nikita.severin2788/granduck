﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Granduck.Audio;
public class RobotNavigation : MonoBehaviour
{
    [SerializeField] private float minVelocity = 1f;
    [SerializeField] private float RotationSpeed;
    [SerializeField] private float speed = 5f;
    [SerializeField] private Transform body;
    [SerializeField] private Transform look;

    private RobotAnimations animations;
    private Footsteps footsteps;
    private float startSpeed;
    private Vector3 lastFrameVelocity;
    private Quaternion _lookRotation;
    private Rigidbody RB;
    private bool play = true;
    private bool TargetActive = false;    

    public static bool BounceOff;

    private bool stopped = false;
    private Vector3 target;


    private void OnEnable()
    {
        startSpeed = speed;

        Duck.SetTarget += SetTarget;
        RB = GetComponent<Rigidbody>();
        footsteps = GetComponentInChildren<Footsteps>();
        animations = GetComponent<RobotAnimations>();

        DuckTrigger.StonePunch += StonePunch;
        IDamageble.GameOver += GameOver;
        Door.SetTarget += SetTarget;
    }

    private void OnDisable()
    {
        DuckTrigger.StonePunch -= StonePunch;
        IDamageble.GameOver -= GameOver;

        Duck.SetTarget -= SetTarget;
        Door.SetTarget -= SetTarget;
    }

    public void EnterSlime()
    {
        stopped = true;
        if (footsteps != null)
            footsteps.slime = false;
    }

    public void CarpetSteps(bool active)
    {
        if (footsteps != null)
            footsteps.carpet = active;
    }

    private void StonePunch()
    {
        RB.velocity = Vector3.zero;
    }

    private void GoToTargetPoint(Vector3 point, bool mode)
    {
        SetTarget(point);
        TargetActive = mode;
    }

    private void Sleep()
    {
        GameOver();
        play = false;
    }

    public void GameOver()
    {
        play = false;
        speed = 0f;
        RB.velocity = Vector3.zero;
        RB.constraints = RigidbodyConstraints.FreezeAll;
    }

    public void SetTarget(Vector3 target)
    {
        this.target = target;
        this.target.y = transform.position.y;
    }

    private void FixedUpdate()
    {
        if (!stopped && target != Vector3.zero)
        {
            var oldpos = transform.position;
            transform.position = Vector3.MoveTowards(transform.position, target, speed / 100);
            animations.SetSpeed(Vector3.Distance(oldpos, transform.position));
        }
    }

    private void Update()
    {
        if (!stopped && target != Vector3.zero)
        {
            look.LookAt(target);
            body.rotation = Quaternion.Slerp(body.rotation, look.rotation, Time.deltaTime * RotationSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        var iDamage = collision.gameObject.GetComponent<IDamageble>();

        if (iDamage != null)
        {
            iDamage.SetDamage();
        }

        target = Vector3.zero;
    }
}
