﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plasma : MonoBehaviour
{

    public float Speed;
    [SerializeField]
    private float TimeToDestroy = 5f;

    void Update()
    {
        transform.Translate(transform.forward * Speed * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<IDamageble>() != null)
        {
            collision.transform.GetComponent<IDamageble>().SetDamage();
        }
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Wall"))
            Destroy(gameObject);

    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(TimeToDestroy);
        Destroy(gameObject);
    }
}
