﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    [SerializeField]
    private GameObject _sparksPrefab;

    public GameObject laser;
    public Transform stretching1;
    public Transform stretching2;
    public float time = 0;

    private void Start()
    {
        if (time > 0)
        {
            StartCoroutine(Timer());
        }
    }

    IEnumerator Timer()
    {
        var nTime = new WaitForSeconds(time);
        while (true)
        {
            yield return nTime;
            laser.SetActive(!laser.activeSelf);
        }
    }

    public void Deactivated()
    {
        laser.SetActive(false);
        GetComponent<BoxCollider>().enabled = false;
        StopCoroutine(Timer());
    }

    private void OnTriggerEnter(Collider other)
    {
        var damageble = other.GetComponent<IDamageble>();
        if (damageble)
        {
            GameObject ded = GameObject.FindGameObjectWithTag("Player");
            Debug.Log(ded.name);
            Instantiate(_sparksPrefab, other.transform.position, Quaternion.identity);
            damageble.SetDamage();
            laser.SetActive(false);
        }
    }
}
