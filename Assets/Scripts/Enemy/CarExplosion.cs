﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarExplosion : MonoBehaviour
{
    public GameObject ExplosionPaticale;
    public NavigationPoint Patch;
    public GameObject destroyedBomb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Patch.MoveDisable = true;
            Instantiate(ExplosionPaticale, transform.position, Quaternion.Euler(0, 0, 0));
            other.transform.GetComponent<IDamageble>().SetDamage();

            if (destroyedBomb != null)
                Instantiate(destroyedBomb, transform.position, transform.rotation);

            Destroy(Patch.gameObject);
        }
    }
}
