﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotAnimations : MonoBehaviour
{
    public float rotationalSpeed = 1f; 
    [SerializeField] Material tracks;

    public void SetSpeed(float speed)
    {
        var offset = tracks.mainTextureOffset;
        tracks.mainTextureOffset = new Vector2(speed * rotationalSpeed + offset.x, offset.y);
    }

    private void OnDisable()
    {
        tracks.mainTextureOffset = Vector2.zero;
    }
}
