﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private GameObject explosion;
    [SerializeField] private bool DuckHit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Explosion();
        }
        if (DuckHit)
        {
            if (other.CompareTag("Stone"))
            {
                Explosion();
            }
        }

    }

        void Explosion()
        {
            IDamageble.Instance.SetDamage();
            Instantiate(explosion, transform.position, transform.rotation);

            //if (destroyedBomb != null)
            //    Instantiate(destroyedBomb, transform.position, transform.rotation);

            Destroy(gameObject);
        }
}
