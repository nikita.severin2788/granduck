﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaGun : MonoBehaviour
{

    [Header("PlasmaPrefab")]
    [SerializeField] private GameObject PlasmaPrefab;
    [SerializeField] private GameObject SpawnPoint;

    [Header("Settings")]
    [SerializeField] private float StartDelayTime;
    [SerializeField] private float DelayTime;
    [SerializeField] private float PlasmaSpeed;

    private GameObject SetPlasmaPrefab;

    void Start()
    {
        StartCoroutine("Action");
    }

    IEnumerator Action()
    {
        yield return new WaitForSeconds(StartDelayTime);
        while (true)
        {
            SetPlasmaPrefab = Instantiate(PlasmaPrefab, SpawnPoint.transform.position, SpawnPoint.transform.rotation);
            SetPlasmaPrefab.GetComponent<Plasma>().Speed = PlasmaSpeed;
            yield return new WaitForSeconds(DelayTime);
        }
    }
}
