﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Granduck.Audio;
public class TrapBox : MonoBehaviour
{
    public Trap trap;
    [SerializeField]
    private GameObject TrapObject;

    [SerializeField]
    private Electricity electro;

    [SerializeField]
    private int id;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Stone"))
        {
            //electro.Deactive(id);
            trap.Deactivated();
            Destroy(TrapObject);

        }
    }
}
