﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class SaveManager : MonoBehaviour
{
    public static UnityAction<int> Save;

    public static SaveData saveData;
    public const string saveKey = "LD46_LVLData";

    private void Awake()
    {
        if (PlayerPrefs.HasKey(saveKey))
            saveData = JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString(saveKey));
        else
        {
            saveData = new SaveData();
            saveData.lvlData = new int[0];
        }
    }

    public void SaveLevel(int lvl, int stars)
    {
        List<int> levelData = new List<int>();
        levelData.AddRange(saveData.lvlData);
        if (levelData.Count < lvl)
        {
            int x = lvl - levelData.Count;
            for (int i = 0; i < x; i++)
            {
                levelData.Add(0);
            }
        }
        levelData[lvl - 1] = Mathf.Max(stars, levelData[lvl - 1]);

        saveData.lvlData = levelData.ToArray();
        PlayerPrefs.SetString(saveKey, JsonUtility.ToJson(saveData));
        PlayerPrefs.Save();

        Save?.Invoke(stars);
    }
}

[Serializable]
public class SaveData
{
    public int[] lvlData;
}