﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeWalls : MonoBehaviour
{

    public List<Collider> ColliderPool = new List<Collider>();

    public string GlobalKey;
    public string LocalKey1 = "";
    public string LocalKey2 = "";
    public string LocalKey3 = "";

    public bool isPillar;


    public Vector3 SetScale;
    public Vector3 StartScale;
    public float SpeedLerp;

    private bool SetSettings;

    private bool FadeOnStart = true;


    private void OnEnable()
    {
        CameraRig.EnabledUpdate += CheckCameraPostion;
    }
    private void OnDisable()
    {
        CameraRig.EnabledUpdate -= CheckCameraPostion;
    }

    private void Awake()
    {
        StartScale = transform.localScale;
        SetScale = StartScale;
    }

    void Start()
    {
        CreateColliderPool();

        LocalKey1 += " " + GlobalKey;
        LocalKey2 += " " + GlobalKey;
        LocalKey3 += " " + GlobalKey;

        // StartScale = transform.localScale;
        // SetScale = StartScale;
        CheckCameraPostion(PlayerPrefs.GetInt("SelectLocalPositionIndex"), PlayerPrefs.GetInt("SelectGlobalPositionIndex"));

    }

    void CreateColliderPool()
    {
        if (!isPillar)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).GetComponent<Collider>() != null)
                {
                    ColliderPool.Add(transform.GetChild(i).GetComponent<Collider>());
                }
            }
        }
    }

    void EnableAllColliderOnWall()
    {
        for (int i = 0; i < ColliderPool.Count; i++)
        {
            if (ColliderPool[i] != null)
            ColliderPool[i].GetComponent<Collider>().enabled = true;
        }
    }

    void DisableAllColliderOnWall()
    {
        for (int i = 0; i < ColliderPool.Count; i++)
        {
            if (ColliderPool[i] != null)
            ColliderPool[i].GetComponent<Collider>().enabled = false;
        }
    }

    void Update()
    {
        if (!isPillar)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, SetScale, SpeedLerp * Time.deltaTime);
        }
    }
    
    private void CheckCameraPostion(int LocalPosition, int GlobalPosition)
    {
        string InputKey = LocalPosition.ToString() + " " + GlobalPosition.ToString();


        if (isPillar)
        {
            if (InputKey != LocalKey1 && InputKey != LocalKey2 && InputKey != LocalKey3)
            {
                SetVisible();
            }
            else
            {
                if (FadeOnStart)
                {
                    InvisibleOnStart();
                    FadeOnStart = false;
                }
                else
                {
                    SetInvisble();
                }

            }
        }
        else
        {
            if (InputKey != LocalKey1 && InputKey != LocalKey2)
            {
                SetVisible();
            }
            else
            {
                if (FadeOnStart)
                {
                    InvisibleOnStart();
                    FadeOnStart = false;
                }
                else
                {
                    SetInvisble();
                }
            }
        }
        FadeOnStart = false;
}

    public void SetInvisble()
    {
        if (isPillar)
        {

            gameObject.transform.GetComponent<MeshRenderer>().enabled = false;
            gameObject.transform.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            SetScale = Vector3.zero;
            DisableAllColliderOnWall();
        }
    }


    public void SetVisible()
    {
        //Debug.Log("Visible");
        if (isPillar)
        {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            SetScale = StartScale;
            EnableAllColliderOnWall();

        }
    }

    public void InvisibleOnStart()
    {
        //Debug.Log("Invisible");
        if (isPillar)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            SetScale = Vector3.zero;
            transform.localScale = Vector3.zero;
        }
    }
}
