﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraController : MonoBehaviour
{
    [SerializeField] private GameObject LeftButton;
    [SerializeField] private GameObject RightButton;
    [SerializeField] private GameObject RestartButton;
    [SerializeField] private GameObject DuckCountUI;
    [SerializeField] private bool DedIsDead;


    private void OnEnable()
    {
        ThrowCursor.NewThrowActive += UIEnabled;
        IDamageble.GameOver += UIOff;
        Bed.DedInBad += UIOff;
    }

    private void OnDisable()
    {
        ThrowCursor.NewThrowActive -= UIEnabled;
        IDamageble.GameOver -= UIOff;
        Bed.DedInBad -= UIOff;
    }

    private void UIOff()
    {
        LeftButton.SetActive(false);
        RightButton.SetActive(false);
        DuckCountUI.SetActive(false);
        RestartButton.SetActive(false);
        DedIsDead = true;
    }

    private void UIEnabled(bool EnabledMode)
    {
        if (!DedIsDead)
        {
            if (!EnabledMode)
            {
                LeftButton.SetActive(true);
                RightButton.SetActive(true);
            }
            else
            {
                LeftButton.SetActive(false);
                RightButton.SetActive(false);
            }
        }
    }
}
