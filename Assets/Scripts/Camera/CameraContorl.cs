﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraContorl : MonoBehaviour
{
    public bool NextAngleMode;
    public bool BackAngleMode;

    public void NextAngle()
    {
        if(NextAngleMode)
        CameraRig.Instance.NextLocalAngle();
    }

    public void BackAngle()
    {
        if(BackAngleMode)
        CameraRig.Instance.BackLocalAngle();
    }
}
