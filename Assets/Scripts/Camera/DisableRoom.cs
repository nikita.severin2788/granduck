﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRoom : MonoBehaviour
{
    [SerializeField] int RoomIndex;

    private void OnEnable()
    {
        CameraRig.EnabledUpdate += Mode;
    }

    private void OnDisable() {
        CameraRig.EnabledUpdate -= Mode;
    }

    private void Mode(int Local, int Global)
    {
        if(Global == RoomIndex)
            GetComponent<MeshRenderer>().enabled = false;
        else
            GetComponent<MeshRenderer>().enabled = true;

    }
}
