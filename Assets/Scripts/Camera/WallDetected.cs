﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetected : MonoBehaviour
{
    [SerializeField]
    private ThrowCursor TCursor;

    //private int Layermask = 1 << 15;
    private RaycastHit hit;
    void Start()
    {
        TCursor.TargetTransform += RaycastDetected;
    }

    void Update()
    {
        
    }

    void RaycastDetected(Transform _transform)
    {
        Debug.DrawRay(_transform.position, _transform.forward, Color.red);
        if (Physics.Raycast(_transform.position, _transform.forward, out hit, 500, 1<<18))
        {
            if (hit.transform.GetComponent<InvisibleWall>() != null)
            {
                //hit.transform.GetComponent<InvisibleWall>().WallDisabled();
            }
        }

    }
}
