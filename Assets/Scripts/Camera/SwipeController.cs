﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SwipeController : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    public static event UnityAction Swipe;

    private ThrowCursor throwCursor;

    public int moreX = 3;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.x) > moreX)
        {
            //Debug.Log(throwCursor);
            //Debug.Log(eventData.position);

            if (throwCursor != null)
                if (throwCursor.OnSet())
                    return;

            if (eventData.delta.x > 0)
            {
                CameraRig.Instance.BackLocalAngle();
                Swipe?.Invoke();
            }
            else
            {
                CameraRig.Instance.NextLocalAngle();
                Swipe?.Invoke();
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }

    private void Awake()
    {
        throwCursor = FindObjectOfType<ThrowCursor>();
    }
}
