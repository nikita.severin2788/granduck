﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class CameraRig : MonoBehaviour
{
    public static CameraRig Instance;

    public static UnityAction<int, int> EnabledUpdate;

    public static UnityAction WallOff;

    public static UnityAction Rotation;

    //RotationData
    [SerializeField] private Vector3 CameraRotationGlobal;
    [SerializeField] private Vector3 CameraRotationLocal;
    [SerializeField] private float GlobalAngleY;
    [SerializeField] private float LocalAngleY;
    [SerializeField] private float SpeedRotation = 0.05f;

    //Mods
    bool DedIsAlive = true;
    bool WinGameMode = false;

    //Links
    public GameObject CamraObject;

    [SerializeField]
    public GameObject SubCameraRig;

    //Data
    [SerializeField]
    private List<CameraRotationData> RotationData = new List<CameraRotationData>();

    [SerializeField]
    private CameraRotationData SelectCameraRotationData;

    //Index's
    private int GlobalPositionIndex;
    public int SelectGlobalPositionIndex;
    private int LocalPositionIndex;
    public int SelectLocalPositionIndex;

    //Sounds
    [SerializeField]
    private AudioClip _whoosh;

    [SerializeField]
    private AudioSource _source;

    RaycastHit ray;

    bool PauceMode;


    private void Awake()
    {
        Instance = this;

        if(PlayerPrefs.GetInt("LastLevelIndex") != SceneManager.GetActiveScene().buildIndex) {
            PlayerPrefs.SetInt("LastLevelIndex",SceneManager.GetActiveScene().buildIndex);
            CleanAngleData();
        }
    }

    private void OnEnable()
    {
        IDamageble.GameOver += GameOver;
        Bed.DedInBad += WinGame;
    }

    private void OnDisable()
    {
        IDamageble.GameOver -= GameOver;
        Bed.DedInBad -= WinGame;
    }

    public void GameOver()
    {
        DedIsAlive = false;
    }

    public void WinGame() {
        WinGameMode = true;
    }

    private void Start()
    {
        
        LoadAngleData();
        WallOff?.Invoke();
        SelectData();
        LevelManager.Singleton.PauceEvent += Pauce;
        Pauce(false);
        EnabledUpdate(SelectGlobalPositionIndex,SelectLocalPositionIndex);
        
    }

    void Update()
    {
        if (DedIsAlive && !PauceMode && !WinGameMode)
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Q))
                BackGlobalAngle();

            if (Input.GetKeyDown(KeyCode.E))
                NextGlobalAngle();

            if (Input.GetKeyDown(KeyCode.C))
                PlayerPrefs.DeleteAll();
#endif
            if (Input.GetKeyDown(KeyCode.A))
                BackLocalAngle();

            if (Input.GetKeyDown(KeyCode.D))            
                NextLocalAngle();
        }

        CameraRotationGlobal = Vector3.Lerp(CameraRotationGlobal, new Vector3(CameraRotationGlobal.x, GlobalAngleY, CameraRotationGlobal.z), SpeedRotation * Time.deltaTime);
        transform.rotation = Quaternion.Euler(CameraRotationGlobal.x, CameraRotationGlobal.y, CameraRotationGlobal.z);

        CameraRotationLocal = Vector3.Lerp(CameraRotationLocal, new Vector3(CameraRotationLocal.x, LocalAngleY, CameraRotationLocal.z), SpeedRotation * Time.deltaTime);
        SubCameraRig.transform.localRotation = Quaternion.Euler(CameraRotationLocal.x, CameraRotationLocal.y, CameraRotationLocal.z);
    }

    public void SelectData()
    {
        SelectCameraRotationData = RotationData[SelectGlobalPositionIndex];
    }

    private void Pauce(bool state)
    {
        PauceMode = state;
    }

    public IEnumerator NextRoom()
    {
        if (SelectLocalPositionIndex != 0)
        {
            Pauce(true);
            SubCameraRig.transform.rotation = Quaternion.Euler(0, 90f, 0);
            CameraRotationLocal = new Vector3(0, 90f, 0);
            LocalAngleY = 0;
            LocalPositionIndex = 0;
            SelectLocalPositionIndex = 0;
            EnabledUpdate?.Invoke(SelectLocalPositionIndex, SelectGlobalPositionIndex);
            yield return new WaitForSeconds(0.1f);
        }
        Pauce(true);
        CameraRotationLocal = new Vector3(0, 0, 0);
        LocalAngleY = 0;
        NextGlobalAngle();
        yield return new WaitForSeconds(0.4f);
        Pauce(false);
    }

    //--------GlovalRotationFunctions-----------
    public void NextGlobalAngle()
    {
        SelectGlobalPositionIndex = NextGlobalIndex();
        while (RotationData[SelectGlobalPositionIndex].IgnorPosition == true)
        {
            SelectGlobalPositionIndex = NextGlobalIndex();
            GlobalAngleY -= 90f;
        }
        if (RotationData[SelectGlobalPositionIndex].IgnorPosition == false)
        {

            LocalPositionIndex = 0;
            SelectLocalPositionIndex = 0;
            GlobalAngleY -= 90f;
            EnabledUpdate?.Invoke(SelectLocalPositionIndex, SelectGlobalPositionIndex);
            LocalAngleY = 0f;
            SelectData();
            SaveAngleData();
        }

        Rotation?.Invoke();
    }

    public void BackGlobalAngle()
    {
        SelectGlobalPositionIndex = BackGlobalIndex();
        while (RotationData[SelectGlobalPositionIndex].IgnorPosition == true)
        {
            SelectGlobalPositionIndex = BackGlobalIndex();
            GlobalAngleY += 90f;
        }
        if (RotationData[SelectGlobalPositionIndex].IgnorPosition == false)
        {

            GlobalAngleY += 90f;
            LocalPositionIndex = 0;
            SelectLocalPositionIndex = 0;
            EnabledUpdate?.Invoke(SelectLocalPositionIndex, SelectGlobalPositionIndex);
            LocalAngleY = 0f;
            SelectData();
            SaveAngleData();
        }

        Rotation?.Invoke();
    }

    private int NextGlobalIndex()
    {
        _source.PlayOneShot(_whoosh);
        GlobalPositionIndex++;
        if (RotationData.Count - 1 < GlobalPositionIndex)
        {
            GlobalPositionIndex = 0;
            return GlobalPositionIndex;
        }

        if (1 > GlobalPositionIndex)
        {
            GlobalPositionIndex = RotationData.Count;
            return GlobalPositionIndex;
        }
        return GlobalPositionIndex;
    }

    private int BackGlobalIndex()
    {
        _source.PlayOneShot(_whoosh);
        GlobalPositionIndex--;
        if (RotationData.Count - 1 < GlobalPositionIndex)
        {
            GlobalPositionIndex = 0;
            return GlobalPositionIndex;
        }

        if (0 > GlobalPositionIndex)
        {
            GlobalPositionIndex = RotationData.Count - 1;
            return GlobalPositionIndex;
        }
        return GlobalPositionIndex;
    }
    //---------LocalRotationFunctions-----------
    public void NextLocalAngle()
    {
        SelectLocalPositionIndex = NextLocalIndex();
        while (SelectCameraRotationData.LocalData[SelectLocalPositionIndex].IgnorPosition == true)
        {
            SelectLocalPositionIndex = NextLocalIndex();
            LocalAngleY -= 90f;
        }
        if (SelectCameraRotationData.LocalData[SelectLocalPositionIndex].IgnorPosition == false)
        {
            LocalAngleY -= 90f;
            EnabledUpdate?.Invoke(SelectLocalPositionIndex, SelectGlobalPositionIndex);
            SaveAngleData();
        }

        Rotation?.Invoke();
    }

    public void BackLocalAngle()
    {
        SelectLocalPositionIndex = BackLocalIndex();
        while (SelectCameraRotationData.LocalData[SelectLocalPositionIndex].IgnorPosition == true)
        {
            SelectLocalPositionIndex = BackLocalIndex();
            LocalAngleY += 90f;
        }
        if (SelectCameraRotationData.LocalData[SelectLocalPositionIndex].IgnorPosition == false)
        {
            LocalAngleY += 90f;
            EnabledUpdate?.Invoke(SelectLocalPositionIndex,SelectGlobalPositionIndex);
            SaveAngleData();
        }

        Rotation?.Invoke();
    }

    private int NextLocalIndex()
    {
        _source.PlayOneShot(_whoosh);
        LocalPositionIndex++;
        if (SelectCameraRotationData.LocalData.Count - 1 < LocalPositionIndex)
        {
            LocalPositionIndex = 0;
            return LocalPositionIndex;
        }

        if (1 > LocalPositionIndex)
        {
            LocalPositionIndex = SelectCameraRotationData.LocalData.Count;
            return LocalPositionIndex;
        }
        return LocalPositionIndex;
    }
    private int BackLocalIndex()
    {
        _source.PlayOneShot(_whoosh);
        LocalPositionIndex--;
        if (SelectCameraRotationData.LocalData.Count - 1 < LocalPositionIndex)
        {
            LocalPositionIndex = 0;
            return LocalPositionIndex;
        }

        if (0 > LocalPositionIndex)
        {
            LocalPositionIndex = SelectCameraRotationData.LocalData.Count - 1;
            return LocalPositionIndex;
        }
        return LocalPositionIndex;
    }

    void SaveAngleData()
    {
        PlayerPrefs.SetInt("LocalPositionIndex", LocalPositionIndex);
        PlayerPrefs.SetInt("GlobalPositionIndex", GlobalPositionIndex);
        PlayerPrefs.SetInt("SelectGlobalPositionIndex", SelectGlobalPositionIndex);
        PlayerPrefs.SetInt("SelectLocalPositionIndex", SelectLocalPositionIndex);
        PlayerPrefs.SetFloat("LocalAngleY", LocalAngleY);
        PlayerPrefs.SetFloat("GlobalAngleY", GlobalAngleY);
    }

    void LoadAngleData()
    {
       LocalPositionIndex = PlayerPrefs.GetInt("LocalPositionIndex");
       GlobalPositionIndex = PlayerPrefs.GetInt("GlobalPositionIndex");
       SelectGlobalPositionIndex = PlayerPrefs.GetInt("SelectGlobalPositionIndex");
       SelectLocalPositionIndex = PlayerPrefs.GetInt("SelectLocalPositionIndex");
       SubCameraRig.transform.rotation = Quaternion.Euler(0,PlayerPrefs.GetFloat("LocalAngleY"),0);
       transform.rotation = Quaternion.Euler(0,PlayerPrefs.GetFloat("GlobalAngleY"),0);
       CameraRotationGlobal.y = PlayerPrefs.GetFloat("GlobalAngleY");
       GlobalAngleY = PlayerPrefs.GetFloat("GlobalAngleY");
       LocalAngleY = PlayerPrefs.GetFloat("LocalAngleY");
       CameraRotationLocal.y = PlayerPrefs.GetFloat("LocalAngleY");
       EnabledUpdate?.Invoke(SelectLocalPositionIndex, SelectGlobalPositionIndex);
    }

    public void CleanAngleData()
    {
        PlayerPrefs.SetInt("LocalPositionIndex", 0);
        PlayerPrefs.SetInt("GlobalPostionIndex", 0);
        PlayerPrefs.SetInt("SelectGlobalPositionIndex", 0);
        PlayerPrefs.SetInt("SelectLocalPositionIndex", 0);
        PlayerPrefs.SetFloat("LocalAngleY", 0);
        PlayerPrefs.SetFloat("GlobalAngleY", 0);
    }
}

[System.Serializable]
public class CameraRotationData {

    public int PostionIndex;
    public bool IgnorPosition;
   // public float RotationAngleY;
    public List <CameraLocalRotation> LocalData = new List<CameraLocalRotation>();

    public CameraRotationData(int _positionIndex, bool _ignorPosition, float _rotationAngleY , List <CameraLocalRotation> _localData)
    {
        PostionIndex = _positionIndex;
        IgnorPosition = _ignorPosition;
       // RotationAngleY = _rotationAngleY;
        LocalData = _localData;
    }

}

[System.Serializable]
public class CameraLocalRotation{

    public int PostionIndex;
    public bool IgnorPosition;
    //public float RotationAngleY;

    public CameraLocalRotation(int _positionIndex, bool _ignorPosition, float _rotationAngleY)
    {
        PostionIndex = _positionIndex;
        IgnorPosition = _ignorPosition;
       // RotationAngleY = _rotationAngleY;
    }
}

