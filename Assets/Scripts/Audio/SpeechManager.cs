﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Granduck.Audio
{
    public class SpeechManager : MonoBehaviour
    {
        [Header("Sounds")]
        [SerializeField] private AudioClip[] _deathSounds;
        [SerializeField] private AudioClip[] _actionSounds;
        [SerializeField] private AudioClip[] _randomSounds;
        [SerializeField] private AudioClip[] _hitSounds;
        [SerializeField] private AudioSource _source;

        [Header("IDS")]
        [SerializeField] private int _lastAction;
        [SerializeField] private int _lastRandom;
        [SerializeField] private int _lastHit;

        [Header("States")]
        [SerializeField] private bool _isPlaying;

        private void Start()
        {
            Invoke("RandomSound", 5f);
            _lastAction = -1;
            _lastRandom = -1;
        }

        private void OnEnable()
        {
            DuckTrigger.StonePunch += Hit;
            IDamageble.GameOver += Death;
            Duck.SetTarget += Action;
        }

        private void OnDisable()
        {
            DuckTrigger.StonePunch -= Hit;
            IDamageble.GameOver -= Death;
            Duck.SetTarget -= Action;
        }

        public void Action(Vector3 noUsing)
        {
            if (!_isPlaying)
            {
                int id = RandNoRepeat(_lastAction, _actionSounds.Length);
                _lastAction = id;
                StartCoroutine(PlaySound(_actionSounds[id]));
            }
        }

        public void Death()
        {
            if (_isPlaying) _source.Stop();
            StartCoroutine(PlaySound(_deathSounds[Random.Range(0, _deathSounds.Length)]));
        }

        public void Hit()
        {
            if (_isPlaying) _source.Stop();
            int id = RandNoRepeat(_lastHit, _hitSounds.Length);
            _lastHit = id;
            StartCoroutine(PlaySound(_hitSounds[id]));
        }

        private void RandomSound()
        {
            if (!_isPlaying)
            {
                int id = RandNoRepeat(_lastRandom, _randomSounds.Length);
                _lastRandom = id;
                StartCoroutine(PlaySound(_randomSounds[id]));
            }
            Invoke("RandomSound", Random.Range(10, 20));
        }


        private int RandNoRepeat(int last, int max)
        {
            int id;
            do
            {
                id = Random.Range(0, max);
            } while (id == last);

            return id;
        }

        private IEnumerator PlaySound(AudioClip clip)
        {
            _isPlaying = true;
            _source.PlayOneShot(clip);
            yield return new WaitForSeconds(clip.length);
            _isPlaying = false;
        }
    }
}
