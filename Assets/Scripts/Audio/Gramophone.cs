﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gramophone : MonoBehaviour
{
    [SerializeField]
    private AudioSource _normal;

    [SerializeField]
    private AudioSource _damaged;

    [SerializeField]
    private float _fadeTime;

    [SerializeField]
    private bool _normalIsPlaying;

    [SerializeField]
    private bool _isFading;

    private void Start()
    {
        _normalIsPlaying = true;

        _normal = GameObject.Find("Game Music").GetComponent<AudioSource>();
        _damaged = GameObject.Find("Game Music Broken").GetComponent<AudioSource>();

        if (_normal.volume == 0) StartCoroutine(Fading(false));

    }

    public void Fade()
    {
        StartCoroutine(Fading(_normalIsPlaying));
    }

    private IEnumerator Fading(bool damage)
    {
        _isFading = true;
        _normalIsPlaying = !damage;


        AudioSource _sourceFadeIn;
        AudioSource _sourceFadeOut;

        if (damage)
        {
            _sourceFadeIn = _damaged;
            _sourceFadeOut = _normal;
        }
        else
        {
            _sourceFadeIn = _normal;
            _sourceFadeOut = _damaged;
        }

        float t = 0;
        float curLerpTime = 0;
        while (curLerpTime < _fadeTime)
        {
            curLerpTime += Time.deltaTime;
            t = curLerpTime / _fadeTime;
            _sourceFadeIn.volume = Mathf.Lerp(0, 1, t);
            _sourceFadeOut.volume = Mathf.Lerp(1, 0, t);

            yield return null;
        }
        _isFading = false;
    }
}
