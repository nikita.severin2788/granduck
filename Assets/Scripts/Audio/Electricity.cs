﻿using UnityEngine;

namespace Granduck.Audio
{
    public class Electricity : MonoBehaviour
    {

        [SerializeField]
        private int _count1;

        [SerializeField]
        private int _count2;

        public void Deactive(int id)
        {
            if (id == 0) _count1++;
            else _count2++;
            if (_count1 >= 1 && _count2 >= 1) gameObject.SetActive(false);
        }
    }
}
