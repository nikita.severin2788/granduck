﻿using UnityEngine;
using Granduck.Audio;
public class SettingsSaver : MonoBehaviour
{
	public void Save()
	{
		Granduck.Audio.AudioSettings.instance.SaveSettings();
	}
}
