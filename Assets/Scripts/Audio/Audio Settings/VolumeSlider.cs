﻿using UnityEngine;
using UnityEngine.UI;
using Granduck.Audio;

public class VolumeSlider : MonoBehaviour
{
	[SerializeField] private AudioGroup _audioGroup;
	private float _lastValue;
	//Components
	private Slider _slider;


	private void Awake()
	{
		_slider = GetComponent<Slider>();
	}
	private void OnEnable()
	{
		_slider.value = Granduck.Audio.AudioSettings.instance.GetVolume(_audioGroup);
	}
	public void Set()
	{
		if(_lastValue != _slider.value)
		{
			_lastValue = _slider.value;
			Granduck.Audio.AudioSettings.instance.SetVolume(_audioGroup, _slider.value);
		}
	}
}
