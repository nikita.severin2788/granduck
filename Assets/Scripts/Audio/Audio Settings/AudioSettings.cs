﻿using UnityEngine;
using UnityEngine.Audio;

namespace Granduck.Audio
{
	public enum AudioGroup
	{
		Music,
		Sounds,
		Grandpa
	}
	public class AudioSettings : MonoBehaviour
	{
		[SerializeField] private AudioMixerGroup _mixer;
		public static AudioSettings instance;
		public VolumeSettings currentSettings;

		private void Awake()
		{
			//Singleton
			if(instance == null)
			{
				instance = this;
			}
			else
			{
				Destroy(gameObject);
			}

			//SetSettings(DataSave.LoadSettings());
		}
		private void Start()
		{
			SetSettings(DataSave.LoadSettings());
		}

		public float GetVolume(AudioGroup audioGroup)
		{
			//Hardcode here is fine I think
			if(audioGroup == AudioGroup.Music)
			{
				return currentSettings.music;
			}
			else if(audioGroup == AudioGroup.Grandpa)
			{
				return currentSettings.grandpa;
			}
			else
			{
				return currentSettings.sounds;
			}

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupName"></param>
		/// <param name="value">from 0 to 1</param>
		public void SetVolume(AudioGroup audioGroup, float value)
		{
			Debug.Log(audioGroup.ToString());
			//_mixer.SetFloat(audioGroup.ToString(), Mathf.Log10(Mathf.Clamp(value, 0.0001f, 1f)) * 20);
			_mixer.audioMixer.SetFloat(audioGroup.ToString(), Mathf.Log10(Mathf.Clamp(value, 0.0001f, 1f)) * 20);
			//_mixer.audioMixer.SetFloat("Sounds", -20f);
			if (audioGroup == AudioGroup.Music)
			{
				currentSettings.music = value;
			}
			else if (audioGroup == AudioGroup.Grandpa)
			{
				currentSettings.grandpa = value;
			}
			else
			{
				currentSettings.sounds = value;
			}
		}

		private void SetSettings(VolumeSettings settings)
		{
			currentSettings = settings;
			SetVolume(AudioGroup.Grandpa, settings.grandpa);
			SetVolume(AudioGroup.Music, settings.music);
			SetVolume(AudioGroup.Sounds, settings.sounds);
		}

		public void SaveSettings()
		{
			DataSave.SaveSettings(currentSettings);
		}
	}
}

