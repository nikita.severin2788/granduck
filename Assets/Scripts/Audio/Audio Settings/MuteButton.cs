﻿using UnityEngine;
using UnityEngine.UI;
using Granduck.Audio;
public class MuteButton : MonoBehaviour
{
	[Header("Audio")]
	[SerializeField] private bool _mute;
	private bool Mute
	{
		get
		{
			return _mute;
		}
		set
		{
			_mute = value;
			_image.sprite = value ? _enabledSprite : _disabledSprite;
		}
	}
	[SerializeField] private AudioGroup _audioGroup;

	[Header("Sprites")]
	[SerializeField] private Sprite _enabledSprite;
	[SerializeField] private Sprite _disabledSprite;

	[Header("Components")]
	[SerializeField] private Image _image;

	private void OnEnable()
	{
		LoadState();
	}
	public void SetState()
	{
		Mute = !Mute;
		Granduck.Audio.AudioSettings.instance.SetVolume(_audioGroup, Mute ? 1f : 0f);
	}
	private void LoadState()
	{
		Mute = Granduck.Audio.AudioSettings.instance.GetVolume(_audioGroup) >= 1f;
	}
}
