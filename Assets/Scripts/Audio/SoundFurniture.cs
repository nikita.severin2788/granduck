﻿using UnityEngine;
namespace Granduck.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundFurniture : DuckTarget
    {
        [Header("Sounds")]
        [SerializeField] protected AudioClip[] _hitSounds;
        protected int _lastSoundIndex;

        //Components
        protected AudioSource _source;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
            gameObject.layer = 15;
        }

        private void Start()
        {
            _lastSoundIndex = Random.Range(0, _hitSounds.Length);
        }

        /// <summary>
        /// Whenever duck touches an object
        /// </summary>
        public override void Touch(Transform stone)
        {
            //Choose random sound that wasn't played last time
            int index;
            do
            {
                index = Random.Range(0, _hitSounds.Length);
            } while (_lastSoundIndex == index);

            _source.PlayOneShot(_hitSounds[index]);

            _lastSoundIndex = index;
        }
    }
}
