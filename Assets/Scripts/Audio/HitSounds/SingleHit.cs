﻿using UnityEngine;
namespace Granduck.Audio
{
    public class SingleHit : DuckTarget
    {
        //Components
        private AudioSource _source;

        [Header("Sound")]
        [SerializeField] protected AudioClip _hitSound;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
        }

        public override void Touch(Transform stone)
        {
            _source.PlayOneShot(_hitSound);
        }
    }
}
