﻿using UnityEngine;

namespace Granduck.Audio
{
    public class Music : MonoBehaviour
    {
        private static Music music;
        public bool menu;

        private void OnEnable()
        {
            if (menu)
            {
                Destroy(music.gameObject);
                music = this;
            }
            else
            {
                if (music == null)
                {
                    music = this;
                    gameObject.transform.parent = null;
                    DontDestroyOnLoad(gameObject);
                }
                else Destroy(gameObject);
            }
        }
    }
}