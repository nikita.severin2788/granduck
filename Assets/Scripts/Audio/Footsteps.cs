﻿using UnityEngine;

namespace Granduck.Audio
{
    public class Footsteps : MonoBehaviour
    {
        [SerializeField] private bool _onStart;
        [SerializeField] private AudioClip[] _stepSounds;
        [SerializeField] private AudioClip[] _stepInSlimeSounds;
        [SerializeField] private AudioClip[] _stepInCarpet;
        [SerializeField] private AudioSource _source;

        private int _lastID;
        public bool slime = false;
        public bool carpet = false;

        private void Start()
        {
            _lastID = Random.Range(0, _stepSounds.Length);
            if (_onStart) Step();
        }

        public void Step()
        {
            var sound = slime ? _stepInSlimeSounds : carpet ? _stepInCarpet : _stepSounds;

            int id;
            do
            {
                id = Random.Range(0, sound.Length);
            } while (_lastID == id);
            _source.PlayOneShot(sound[id]);
            _lastID = id;
        }
    }
}
