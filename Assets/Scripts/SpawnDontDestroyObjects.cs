﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDontDestroyObjects : MonoBehaviour
{
    public List<GameObject> Objects = new List<GameObject>();

    private void Awake() {
        if(Objects.Count != 0) {
            foreach(GameObject SetObject in Objects) {
                Instantiate(SetObject,Vector3.zero,Quaternion.Euler(0,0,0));
            }
        }
    }
}
