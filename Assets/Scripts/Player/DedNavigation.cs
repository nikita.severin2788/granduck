﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Granduck.Audio;
public class DedNavigation : MonoBehaviour
{
    [SerializeField] private float minVelocity = 1f;
    [SerializeField] private float RotationSpeed;
    [SerializeField] private float speed = 1f;
    [SerializeField] private Transform oldman;
    [SerializeField] private GameObject StoneTarget;

    private Footsteps footsteps;
    private float startSpeed;
    private Vector3 lastFrameVelocity;
    private Quaternion _lookRotation;
    private Rigidbody RB;
    private bool play = true;
    private bool TargetActive = false;
    public static bool BounceOff;


    private void OnEnable()
    {
        startSpeed = speed;

        Duck.SetTarget += SetTarget;
        RB = GetComponent<Rigidbody>();
        footsteps = GetComponentInChildren<Footsteps>();

        var ST = Instantiate(StoneTarget, transform.position, transform.rotation);
        ST.GetComponent<DuckTrigger>().ded = transform;

        DuckTrigger.StonePunch += StonePunch;
        Bed.DedInBad += GameWin;
        IDamageble.GameOver += GameOver;
        Door.SetTarget += SetTarget;
       // GoSleepScript.GotToBadAction += GoToTargetPoint;
       // GoSleepScript.Sleep += Sleep;
    }

    private void OnDisable()
    {
        DuckTrigger.StonePunch -= StonePunch;
        Bed.DedInBad -= GameWin;
        IDamageble.GameOver -= GameOver;

        Duck.SetTarget -= SetTarget;
        Door.SetTarget -= SetTarget;
      //  GoSleepScript.GotToBadAction -= GoToTargetPoint;
      //  GoSleepScript.Sleep -= Sleep;
    }

    public void SlimeSteps(float DebuffSpeed)
    {
        speed = startSpeed * DebuffSpeed;
        footsteps.slime = true;
    }

    public void SlimeStepsOff()
    {
        speed = startSpeed;
        footsteps.slime = false;
    }

    public void CarpetSteps(bool active)
    {
        footsteps.carpet = active;
    }

    private void StonePunch()
    {
        RB.velocity = Vector3.zero;
    }

    private void GameWin()
    {
        Destroy(gameObject);
    }

    private void GoToTargetPoint(Vector3 point, bool mode)
    {
        SetTarget(point);
        TargetActive = mode;
    }

    private void Sleep()
    {
        GameOver();
        play = false;
    }

    public void GameOver()
    {
        play = false;
        speed = 0f;
        RB.velocity = Vector3.zero;
        RB.constraints = RigidbodyConstraints.FreezeAll;
    }

    public void SetTarget(Vector3 target)
    {        
        if (play)
        {
            target.y = transform.position.y;
            RB.velocity = (target - transform.position).normalized * speed;
        }
        if (!play && TargetActive)
        {
            target.y = transform.position.y;
            RB.velocity = (target - transform.position).normalized * speed;
        }
    }    

    private void Update()
    {
        if (play && RB.velocity != Vector3.zero)
        {
            RB.velocity = Vector3.Normalize(RB.velocity) * speed;

            _lookRotation = Quaternion.LookRotation(RB.velocity);
            oldman.rotation = Quaternion.Slerp(oldman.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
            lastFrameVelocity = RB.velocity;
        }
    }   

    private void OnCollisionEnter(Collision collision)
    {
        if(!BounceOff)
            Bounce(collision.contacts[0].normal);
    }

    private void Bounce(Vector3 collisionNormal)
    {
        var localSpeed = lastFrameVelocity.magnitude;
        var direction = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);
        RB.velocity = direction * Mathf.Max(localSpeed, minVelocity);
        RB.velocity = RB.velocity.normalized * speed;
    }
}
