﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DedAnimationController : MonoBehaviour
{
    public Animator animator;
    public Rigidbody RB;

    [SerializeField]
    private bool _isPauce;


    private void OnEnable()
    {
        DuckTrigger.StonePunch += StonePunch;
        IDamageble.GameOver += Dead;
       // GoSleepScript.SleepPostion += Sleep;
        
    }

    private void OnDisable()
    {
        DuckTrigger.StonePunch -= StonePunch;
        IDamageble.GameOver -= Dead;
       // GoSleepScript.SleepPostion -= Sleep;
    }

    private void StonePunch()
    {
        animator.SetTrigger("StonePunch");
    }

    void Dead()
    {
        animator.SetTrigger("DedIsDead");
    }

    void Sleep(string postion)
    {
        if (postion == "Left")
            animator.SetTrigger("SleepLeft");

        if (postion == "Right")
            animator.SetTrigger("SleepRight");
    }

    private void Update()
    {
        if (!_isPauce)
            animator.SetFloat("Speed", Vector3.Magnitude(RB.velocity) / 3);
    }
}
