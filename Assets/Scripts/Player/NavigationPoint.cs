﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationPoint : MonoBehaviour
{
    [Header("ObjectLink")]
    public Transform ChildTransform;
    [Header("Points")]
    public List<GameObject> ObjectPoints = new List<GameObject>();
    [Header("SpeedSetting")]
    public float MovingSpeed;
    public float RotationSpeed;
    [Header("Mode")]
    public bool RotationDisable;
    public bool MoveDisable;

    private bool fadeOn;

    int SetElement = 0;
    private Vector3 Target;

    public void StopMoving()
    {
        RotationDisable = true;
        MoveDisable = true;
    }

    public void Fade()
    {
        fadeOn = true;
    }

    void Update()
    {

        if (fadeOn)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, 7f);
        }

        if (!MoveDisable)
        {
            if (!RotationDisable)
            {
                Quaternion endRotation = Quaternion.LookRotation(ObjectPoints[SetElement].transform.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime * RotationSpeed);
            }
            if (Vector3.Distance(transform.position, ObjectPoints[SetElement].transform.position) < 0.2f)
            {
                if (SetElement == ObjectPoints.Count -1)
                    SetElement = 0;
                else
                    SetElement++;
            }
            transform.position = Vector3.MoveTowards(transform.position, ObjectPoints[SetElement].transform.position, Time.deltaTime * MovingSpeed);
        }
    }
}
