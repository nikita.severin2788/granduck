﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IDamageble : MonoBehaviour
{
    public static event UnityAction GameOver;

    public static IDamageble Instance;

    public bool OneDamage;

    private void Awake()
    {
        Instance = this;
    }

    private void OnDisable()
    {
        //GameOver = null;
    }

    public void SetDamage()
    {
        if(!OneDamage) {
            GameOver?.Invoke();
            OneDamage = true;
        }
    }
}
