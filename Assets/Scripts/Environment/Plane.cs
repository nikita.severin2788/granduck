﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Plane : MonoBehaviour
{
    public static event UnityAction AddStone;

    private void OnTriggerEnter(Collider other)
    {
        var stone = other.GetComponent<Duck>();
        if (stone != null)
        {
            AddStone?.Invoke();
            Destroy(other.gameObject);
        }
    }
}
