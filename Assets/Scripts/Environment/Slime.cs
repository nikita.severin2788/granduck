﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    [Header("SpeedDebuff")]
    public bool SpeedDownMode;
    [Range(0, 1)]
    public float DebuffSpeed;

    [Header("SpeedUp")]
    public bool SpeedUpMode;
    [Range(1, 10)]
    public float SpeedUp;

    [Header("BuffMode")]
    public bool EnableBuff;
    [Range(0,100)]
    public float BuffPercent;

    [Header("AnimatorController")]
    public Animator Anim;
    

    private void OnMouseDown()
    {
        Debug.Log("IsWork");
        if (EnableBuff)
        {
            float BuffCount = BuffPercent / 100f;
            if (DebuffSpeed + BuffCount > 1)
            {
                DebuffSpeed = 1;
            }
            else
            {
                DebuffSpeed = DebuffSpeed + BuffCount;
            }
        }
        Debug.Log(DebuffSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (SpeedDownMode)
            {
                other.gameObject.GetComponent<DedNavigation>().SlimeSteps(DebuffSpeed);
                if (Anim != null)
                    Anim.SetTrigger("SlimeAction");
            }
            if (SpeedUpMode)
            {
                other.gameObject.GetComponent<DedNavigation>().SlimeSteps(SpeedUp);
                if (Anim != null)
                    Anim.SetTrigger("SlimeAction");
            }
        }

        var robot = other.gameObject.GetComponent<RobotNavigation>();
        if (robot != null)
        {
            robot.EnterSlime();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (SpeedDownMode)
            {
                other.gameObject.GetComponent<DedNavigation>().SlimeStepsOff();
                if (Anim != null)
                    Anim.SetTrigger("SlimeDefault");
            }
            if (SpeedUpMode)
            {
                other.gameObject.GetComponent<DedNavigation>().SlimeStepsOff();
                if (Anim != null)
                    Anim.SetTrigger("SlimeDefault");
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var dedNavigation = other.GetComponent<DedNavigation>();
        if (dedNavigation == null) return;

        if (SpeedDownMode)
        {
            dedNavigation.SlimeSteps(DebuffSpeed);
            if (Anim != null)
                Anim.SetTrigger("SlimeAction");
        }
        if (SpeedUpMode)
        {
            dedNavigation.SlimeSteps(SpeedUp);
            if (Anim != null)
                Anim.SetTrigger("SlimeAction");
        }
    }
}
