﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Granduck.Audio
{
    public class Mirror : SoundFurniture
    {
        public GameObject laserObject;
        public Transform point;

        public void LaserHit(Vector3 emitterPosition, Vector3 hitPosition)
        {
            var laser = Instantiate(laserObject, transform).GetComponent<Laser>();
            laser.transform.position = point.position;
            laser.LaserUpdate(GetVector(emitterPosition));
        }

        private Vector3 GetVector(Vector3 emitterPosition)
        {
            var dir = transform.position - emitterPosition;

            if (Mathf.Abs(dir.x) > Mathf.Abs(dir.z))
                return dir.x > 0 ? Func(Vector3.forward, Vector3.back) : Func(Vector3.back, Vector3.forward);
            else
                return dir.z > 0 ? Func(Vector3.right, Vector3.left) : Func(Vector3.left, Vector3.right);
        }

        private Vector3 Func(Vector3 v1, Vector3 v2)
        {
            return transform.rotation.y >= 0 ? v1 : v2;
        }

        public override void Touch(Transform stone)
        {
            int index;
            do
            {
                index = Random.Range(0, _hitSounds.Length);
            } while (_lastSoundIndex == index);

            _source.PlayOneShot(_hitSounds[index]);

            _lastSoundIndex = index;

            var s = stone.GetComponent<Duck>();
            if (s != null)
                if (s.Signal)
                {
                    ChangeRotation();
                    s.ActivateTarget();
                }
        }

        private void ChangeRotation()
        {
            Laser.RestartLaser?.Invoke();

            if (transform.rotation.y > 0)
                transform.Rotate(new Vector3(0, -90, 0));
            else
                transform.Rotate(new Vector3(0, 90, 0));
        }
    }
}
