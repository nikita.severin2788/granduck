﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Door : MonoBehaviour
{
    [SerializeField] Vector3 OpenDoorPosition;
    [SerializeField] Vector3 CloseDoorPosition;
    [SerializeField] float SpeedTransition;
    Vector3 DoorTransiton;
    Vector3 DoorSelectPosition;
    public static UnityAction<Vector3> SetTarget;

    public Transform[] NavigationPoints;

    public void NextRoom()
    {
        CameraRig.Instance.StartCoroutine("NextRoom");
    }

    public void OpenDoor()
    {
        //DoorSelectPosition = OpenDoorPosition;
        //StartCoroutine("MoveToDoor");
       // DedNavigation.BounceOff = true;
    }

    public void CloseDoor()
    {
        //DoorSelectPosition = CloseDoorPosition;
        //sStopCoroutine("MoveToDoor");
       // DedNavigation.BounceOff = false;
    }

    //void Update()
    //{
    //   // DoorTransiton = Vector3.Lerp(DoorTransiton, DoorSelectPosition, SpeedTransition * Time.deltaTime);
    //   // transform.rotation = Quaternion.Euler(DoorTransiton.x, DoorTransiton.y, DoorTransiton.z);


    //}

    public void Point1() { 
        SetTarget?.Invoke(NavigationPoints[0].position);
    }

    public void Point2()
    {
        SetTarget?.Invoke(NavigationPoints[1].position);
    }

    public void PatchNavigation()
    {

    }
}
