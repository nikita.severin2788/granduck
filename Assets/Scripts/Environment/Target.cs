﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Granduck.Audio
{
    [RequireComponent(typeof(AudioSource))]

    public class Target : SoundFurniture
    {
        public static UnityAction TorgetTouch;

        public float SpeedLerp = 2;


        public override void Touch(Transform stone)
        {
            var pos = transform.InverseTransformPoint(stone.position);

            if (pos.z > 0)
            {
                base.Touch(stone);

                TorgetTouch?.Invoke();
                StartCoroutine(Fade());
            }
        }

        IEnumerator Fade()
        {
            var s = new WaitForFixedUpdate();
            float timer = 1f;

            while (timer > 0)
            {
                timer -= Time.deltaTime * SpeedLerp;
                transform.localScale = transform.localScale * timer;
                yield return s;
            }

            Destroy(gameObject);
        }
    }
}
