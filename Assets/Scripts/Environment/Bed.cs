﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bed : MonoBehaviour
{
    public static event UnityAction DedInBad;
    public GameObject dedInBad;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            DedInBad?.Invoke();
            var ded = Instantiate(dedInBad, transform.position, transform.rotation);
            ded.transform.Rotate(90, 0, 0);
        }
    }
}