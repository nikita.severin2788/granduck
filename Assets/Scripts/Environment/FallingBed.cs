﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Granduck.Audio;
public class FallingBed : MonoBehaviour
{
    public int targets = 1;
    private int count;
    public TextMeshPro TextOnWall;
    public GameObject BoxModel;

    private void AddTarget()
    {
        count++;
        TextOnWall.SetText("(" + count + "/7)");
        if (count == targets)
        {
            GetComponent<BoxCollider>().enabled = true;
            BoxModel.SetActive(false);

        }
    }

    private void OnEnable()
    {
        Target.TorgetTouch += AddTarget;
    }

    private void OnDisable()
    {
        Target.TorgetTouch -= AddTarget;
    }
}
