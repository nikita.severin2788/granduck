﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carpet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<DedNavigation>()?.CarpetSteps(true);
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<DedNavigation>()?.CarpetSteps(false);
    }
}
