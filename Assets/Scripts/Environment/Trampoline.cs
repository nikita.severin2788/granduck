﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    [SerializeField] private float bounce = 1f;

    private void Start()
    {
        GetComponent<BoxCollider>().material.bounciness = bounce < 1 ? bounce : 1;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision);
        if (bounce > 1)
        {
            collision.rigidbody.velocity *= bounce;
        }
    }
}
