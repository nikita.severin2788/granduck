﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleWall : MonoBehaviour
{


    public void WallEnebaled() {
        transform.GetComponent<BoxCollider>().isTrigger = false;
    }

    public void WallDisabled() {
        transform.GetComponent<BoxCollider>().isTrigger = true;
    }

    IEnumerator DelayActiveCollider()
    {
        yield return new WaitForSeconds(0);
    }
}
