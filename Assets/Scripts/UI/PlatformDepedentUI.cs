﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace Granduck.UI {
	public class PlatformDepedentUI : MonoBehaviour
	{
		[Header("Platform Variations")]
		[SerializeField] private GameObject _mobileInterface;
		[SerializeField] private GameObject _pcInterface;


		private void Awake()
		{
			_mobileInterface.SetActive(false);
			_pcInterface.SetActive(false);

			//Load platform dependent version of ui
#if UNITY_ANDROID || UNITY_IOS
			_mobileInterface.SetActive(true);
#else
			_pcInterface.SetActive(true);
#endif
		}


	}
	
}

