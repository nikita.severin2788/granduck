﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

namespace Granduck.UI
{
	public class GameUI : MonoBehaviour
	{
		public static GameUI instance;

		[Header("Ducks")]
		[SerializeField] private int _ducks;
		public int Ducks
		{
			get
			{
				return _ducks;
			}
			set
			{
				_ducks = value;
				_ducksText.text = value.ToString();
			}
		}

		[Header("Texts")]
		[SerializeField] private TextMeshProUGUI _ducksText;

		[Header("Panels")]
		[SerializeField] private GameObject _losePanel;
		[SerializeField] private GameObject _winPanel;

		[Header("Sprites")]
		[SerializeField] private Sprite _starSprite;
		[SerializeField] private Sprite _emptyStarSprite;

		[Header("Images")]
		[SerializeField] private Image _star2;
		[SerializeField] private Image _star3;

		private void OnEnable()
		{
			if (instance == null)
			{
				instance = this;
			}
			else
			{
				//Destroy(gameObject);
			}

			_losePanel.SetActive(false);
			_winPanel.SetActive(false);
		}

		public void Lose()
		{
			_losePanel.SetActive(true);
		}


		/// <summary>
		/// Turn on Win Panel and Set amount of win stars
		/// 0 is 1 star; 1 is 2 stars; 2 is 3 stars;
		/// </summary>
		public void Win(int stars)
		{
			DataSave.SaveLevel(SceneManager.GetActiveScene().buildIndex - 1, stars);
			_winPanel.SetActive(true);
			_star2.sprite = (stars > 1) ? _starSprite : _emptyStarSprite;
			_star3.sprite = (stars > 2) ? _starSprite : _emptyStarSprite;
		}
	}
}
