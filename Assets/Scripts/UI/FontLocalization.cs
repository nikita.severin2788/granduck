﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FontLocalization : MonoBehaviour
{
    private Text ObjectText;
    private Font Font;

    public void Start() {
        ChangeFont(FontLocalizationManager.Instance.GetFontStyle());
    }
    private void Awake() {

        
        if(GetComponent<Text>() != null) {
          ObjectText = GetComponent<Text>();
          
        }
    }
    
    private void OnEnable() {
        FontLocalizationManager.ChangeFontEvent += ChangeFont;
        
        

    }

    private void OnDisable() {
        FontLocalizationManager.ChangeFontEvent -= ChangeFont;
    }

    

    public void ChangeFont(Font font) {
        if(ObjectText != null) {
           ObjectText.font = font;
       }
    }
}
