﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDisable : MonoBehaviour
{
    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
