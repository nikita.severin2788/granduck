﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class VolumeSettings
{
	public float sounds;
	public float grandpa;
	public float music;
}
public static class DataSave
{
	#region Levels
	private static readonly string SAVE_PATH = Application.dataPath + "/save.txt";
	public static void SaveLevels(string saveString)
	{
		File.WriteAllText(SAVE_PATH, saveString);
	}
	public static void SaveLevel(int index, int stars)
	{
		List<int> levels = JsonHelper.FromJson<int>(LoadLevels());
		levels[index] = stars;
		SaveLevels(JsonHelper.ToJson<int>(levels, true));
	}
	public static string LoadLevels()
	{
		if (File.Exists(SAVE_PATH))
		{
			return File.ReadAllText(SAVE_PATH);
		}
		else
		{
			return null;
		}
	}

	#endregion

	#region Settings

	private static readonly string SETTINGS_PATH= Application.dataPath + "/settings.txt";
	public static VolumeSettings LoadSettings()
	{
		if (File.Exists(SETTINGS_PATH))
		{
			return JsonUtility.FromJson<VolumeSettings>(File.ReadAllText(SETTINGS_PATH));
		}
		else
		{
			//Default Settings
			VolumeSettings defaultSettings = new VolumeSettings();
			defaultSettings.sounds = 1;
			defaultSettings.grandpa = 1;
			defaultSettings.music = 1;

			SaveSettings(defaultSettings);

			return defaultSettings;
		}
	}

	public static void SaveSettings(VolumeSettings saveSettings)
	{
		File.WriteAllText(SETTINGS_PATH, JsonUtility.ToJson(saveSettings, true));

	}
	#endregion

	
}
