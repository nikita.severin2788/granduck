﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections.Generic;
using System.Linq;
namespace Granduck.UI
{
	public class Levels : MonoBehaviour
	{

		[System.Serializable]
		public class Level
		{
			public Button button;
			public TextMeshProUGUI text;
			public Image icon;
			public Image[] stars;
		} //Level Object


		[Header("Sprites")]
		[SerializeField] private Sprite _lockSprite;
		[SerializeField] private Sprite _playSprite;
		[Header("")]
		[SerializeField] private Sprite _starSprite;
		[SerializeField] private Sprite _emptyStarSprite;

		[Header("UI")]
		[SerializeField] private GameObject _nextButton;
		[SerializeField] private GameObject _prevButton;

		[Header("Pages")]
		[SerializeField] private int _pageAmount = 1; //Number of pages
		[SerializeField] private int _currentPage;
		private int CurrentPage
		{
			get
			{
				return _currentPage;
			}
			set
			{
				_currentPage = value;
				LoadPage();
				_prevButton.SetActive(value > 0);
				_nextButton.SetActive(value < _pageAmount - 1);
			}
		}

		[Header("Level Info")]
		/// <summary>
		/// Info about each level completion. 
		/// 0 is locked. 
		/// 1, 2, 3 are unlocked with 1, 2, 3 stars. 
		/// </summary>
		[SerializeField] private List<int> _levelInfo; //Contains all information about completed and locked levels
		[SerializeField] private Level[] _levels; //Level objects, which contain Text, Icon and Stars
		[SerializeField] private int _maxLevelIndex;

		private void Awake()
		{
			//Initilization
			_maxLevelIndex = _pageAmount * _levels.Length - 1;
			LoadSave();
			CurrentPage = 0;

			//LoadPage();

		}

		private void LoadSave()
		{
			string save = DataSave.LoadLevels();

			if (save != null) //If savefile exists
			{
				_levelInfo = JsonHelper.FromJson<int>(save);

				//If new levels were added
				if ((_levelInfo.Count - 1) < _maxLevelIndex)
				{
					//Fill empty space in savefile with zeros (locked levels)
					_levelInfo.AddRange(Enumerable.Repeat(0, (_maxLevelIndex + 1 - _levelInfo.Count)));
					DataSave.SaveLevels(JsonHelper.ToJson<int>(_levelInfo, true));
				}
			}
			else
			{
				//Create savefile with all levels locked
				_levelInfo.AddRange(Enumerable.Repeat(0, _maxLevelIndex + 1));
				DataSave.SaveLevels(JsonHelper.ToJson<int>(_levelInfo, true));
			}
		}

		//Turn page 
		public void NextPage()
		{
			CurrentPage++;
		}

		public void PrevPage()
		{
			CurrentPage--;
		}

		//Page Load
		private void LoadPage()
		{
			int q = CurrentPage * 10;

			for (int i = 0; i < _levels.Length; i++)
			{
				SetLevel(q + i, _levels[i]);
			}
		}

		//Initilizate level button (set stars or lock or play button)
		//Used in LoadPage function
		private void SetLevel(int index, Level level)
		{
			level.icon.enabled = false;
			level.text.enabled = false;
			level.stars[0].enabled = false;
			level.stars[1].enabled = false;
			level.stars[2].enabled = false;
			level.button.interactable = true;
			int value = _levelInfo[index];

			if (value == 0)
			{
				level.icon.enabled = true;
				bool first = (index == 0);
				if (first || (!first && _levelInfo[index - 1] != 0)) //If level unlocked (but not completed)
				{
					level.icon.sprite = _playSprite;
				}
				else //If level locked
				{
					level.icon.sprite = _lockSprite;
					level.button.interactable = false;
				}
			}

			else //If level unlocked and completed (has stars)
			{
				level.text.enabled = true;
				level.text.text = (index + 1).ToString();
				level.stars[0].enabled = true;
				level.stars[1].enabled = true;
				level.stars[2].enabled = true;

				level.stars[1].sprite = value > 1 ? _starSprite : _emptyStarSprite;
				level.stars[2].sprite = value > 2 ? _starSprite : _emptyStarSprite;
			}
		}

		public void LoadLevel(int localIndex)
		{
			int globalIndex = CurrentPage * 10 + localIndex + 1;
			Debug.Log("Level Load " + globalIndex);
			SceneManager.LoadScene(globalIndex);
		}

	}
}
