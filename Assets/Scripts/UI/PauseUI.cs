﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    public Image img;
    public Text text;
    public Sprite pause;
    public Sprite Duck;
    public GameObject pauseMenu;

    private void OnEnable()
    {
        img.sprite = pause;
        text.text = "";
        ThrowCursor.SetStoneCount += SetText;
    }

    private void OnDisable()
    {
        ThrowCursor.SetStoneCount -= SetText;
    }

    private void SetText(int count)
    {
        img.sprite = Duck;
    }

    public void SetPause()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void Continue()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
