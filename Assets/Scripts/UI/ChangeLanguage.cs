﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeLanguage:MonoBehaviour {
    [SerializeField] private Text TextField;
    [SerializeField] private LanguageTag[] Languages = new LanguageTag[2];
    [SerializeField] private int currentLanguge = 0;

    public void Start() {

        FindeLanguageIndex(Lean.Localization.LeanLocalization.CurrentLanguage);

    }

    public void FindeLanguageIndex(string Language) {
        for(int i = 0; i < Languages.Length; i++) {
            if(Languages[i].name == Language) {
                currentLanguge = i;
                SelectLanguage(i);
            }
        }
    }

    public void NextLanguage() {
        if(currentLanguge + 1 > Languages.Length-1) {
            currentLanguge = 0;
        } else {
            currentLanguge++;
        }
        SelectLanguage(currentLanguge);
    }

    public void BackLanguage() {
        if(currentLanguge - 1 < 0) {
            currentLanguge = Languages.Length-1;
        } else {
            currentLanguge--;
        }
        SelectLanguage(currentLanguge);
    }

    public void SelectLanguage(int index) {
        FontLocalizationManager.Instance.ChangeFont(Languages[index].name);
        Lean.Localization.LeanLocalization.CurrentLanguage = Languages[index].name;

#if UNITY_ANDROID || UNITY_IOS
        TextField.text = Languages[index].shorName;
#else
        TextField.text = Languages[index].localizationName;
#endif
    }
}

[System.Serializable]
public class LanguageTag {

    public string name;
    public string shortName;
    public string localizationName;

    public LanguageTag(string _name,string _shortName,string _localizationName) {
        name = _name;
        shortName = _shortName;
        localizationName = _localizationName;
    }
}
