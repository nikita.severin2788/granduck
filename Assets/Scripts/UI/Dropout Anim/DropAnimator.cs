﻿using UnityEngine;
using UnityEngine.UI;

namespace Granduck.UI
{
	public class DropAnimator : MonoBehaviour
	{
		[SerializeField] private DropAnim[] _icons;
		[SerializeField] private GameObject _openedFolder;
		[SerializeField] private GameObject _closedFolder;
		[SerializeField] private Button _openedButton;
		[SerializeField] private Button _closedButton;

		[SerializeField] private float _animationTime = 0.3f;
		private bool _opened;
		public int FinishCount
		{
			get
			{
				return _finishCount;
			}
			set
			{
				_finishCount = value;
				if (value == _icons.Length)
				{
					Finish();
				}
			}
		}
		private int _finishCount;

		private void Awake()
		{
			_opened = false;
			for (int i = 0; i < _icons.Length; i++)
			{
				_icons[i]._moveTime = _animationTime;
			}
		}
		public void Animate()
		{
			_finishCount = 0;
			_openedButton.interactable = false;
			_closedButton.interactable = false;


			for (int i = 0; i < _icons.Length; i++)
			{
				if (_opened)
				{
					_icons[i].Close();
				}
				else
				{
					_icons[i].Open();
				}
			}
			_opened = !_opened;
		}
		private void Finish()
		{
			_openedButton.interactable = true;
			_closedButton.interactable = true;
			_openedFolder.SetActive(_opened);
			_closedFolder.SetActive(!_opened);
		}
	}
}
