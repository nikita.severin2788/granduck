﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Granduck.UI
{
	public class DropAnim : MonoBehaviour
	{
		[Header("Position")]
		[SerializeField] private Vector2 _closedPosition;
		[SerializeField] private Vector2 _openedPosition;

		[HideInInspector] public float _moveTime;

		[Header("Components")]
		[SerializeField] private DropAnimator _animator;
		private RectTransform _transform;
		private Button _button;



		private void Awake()
		{
			_transform = GetComponent<RectTransform>();
			_button = GetComponent<Button>();
			_transform.anchoredPosition = _closedPosition;
		}

		public void Close()
		{
			StartCoroutine(Move(false));
		}

		public void Open()
		{
			StartCoroutine(Move(true));
		}

		//Animation
		private IEnumerator Move(bool open)
		{
			_button.interactable = false;
			float t = 0;
			float curLerpTime = 0;

			Vector2 pos1 = open ? _closedPosition : _openedPosition;
			Vector2 pos2 = open ? _openedPosition : _closedPosition;

			while (curLerpTime < _moveTime)
			{
				curLerpTime += Time.deltaTime;
				t = curLerpTime / _moveTime;

				t = t * t * (3f - 2f * t);

				_transform.anchoredPosition = Vector2.Lerp(pos1, pos2, t);

				yield return null;
			}
			if (open)
			{
				_button.interactable = true;
			}
			_animator.FinishCount++;
		}
	}
}
