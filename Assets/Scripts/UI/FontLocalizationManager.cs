﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FontLocalizationManager: MonoBehaviour
{

    public static FontLocalizationManager Instance;

    public static UnityAction<Font> ChangeFontEvent;

    public FontData[] Fonts = new FontData[1];

    public Font SelectFontStyle;

    

    public void Awake() {
        Instance = this;
    }

    private void Start() {
        ChangeFont(Lean.Localization.LeanLocalization.CurrentLanguage);
    }

    public Font GetFontStyle() {
        return SelectFontStyle;
    }

    public void ChangeFont(string Langugae) {
        foreach(FontData data in Fonts) {
            if(data.Language == Langugae) {
                SelectFontStyle = data.FontStyle;
                ChangeFontEvent?.Invoke(data.FontStyle);
                Debug.Log("yep");
            }
        }
    }
}

[System.Serializable]
public class FontData{

    public string Language;
    public Font FontStyle;
    public FontData(string _language, Font _font) {
        _font = FontStyle;
        _language = Language;
    }
}
